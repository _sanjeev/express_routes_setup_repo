const express = require("express");
const router = express.Router();

router.use("/login", require("./LoginRoutes"));
router.use("/api/people", require("./PeopleRoute"));

module.exports = router;
