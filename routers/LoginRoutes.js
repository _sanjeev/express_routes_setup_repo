const express = require("express");
const router = express.Router();

router.post("/login", (req, res) => {
    const { name } = req.body;
    if (name) {
        return res.status(200).json({ status: true, message: `Welcome ${name}` });
    }
    return res.status(401).json({ status: false, message: `Unauthorized request` });
});

module.exports = router;
