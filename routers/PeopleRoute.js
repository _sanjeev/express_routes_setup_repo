const express = require("express");
const router = express.Router();

router.get("/", (req, res) => {
    const { name } = req.body;
    if (!name) {
        return res.status(400).json({ status: false, message: "Name is mandatory!" });
    }
    return res.status(200).json({
        status: true,
        data: [
            {
                id: 1,
                name: "Raju",
            },
        ],
    });
});

module.exports = router;
